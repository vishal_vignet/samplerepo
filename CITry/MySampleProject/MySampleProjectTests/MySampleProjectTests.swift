//
//  MySampleProjectTests.swift
//  MySampleProjectTests
//
//  Created by Vishal Karnik on 17/07/18.
//  Copyright © 2018 Vignet Inc. All rights reserved.
//

import XCTest
@testable import MySampleProject

class MySampleProjectTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAddition() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        XCTAssert(20 == MathUtil.add(number1: 10, number2: 10))
    }
    
    func testAdditionNegative() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        XCTAssert(-20 == MathUtil.add(number1: -10, number2: -10))
    }
    
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            //MathUtil.add(number1: 10, number2: 10)
        }
    }
    
}
