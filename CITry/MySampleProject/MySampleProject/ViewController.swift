//
//  ViewController.swift
//  MySampleProject
//
//  Created by Vishal Karnik on 17/07/18.
//  Copyright © 2018 Vignet Inc. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var lblAnswer: UILabel!
    @IBOutlet weak var txtNumber2: UITextField!
    @IBOutlet weak var txtNumber1: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func btnAdd(_ sender: UIButton) {
        
        if let strNumber1=txtNumber1.text , let strNumber2=txtNumber2.text, let number1 = Int(strNumber1), let number2 = Int(strNumber2)
        {
            let result = MathUtil.add(number1 : number1, number2:number2)
            
            lblAnswer.text = result.description
            
        }
        else
        {
            lblAnswer.text = "Please give numbers!"
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
   
    

}

